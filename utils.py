import pandas as pd
import os
import random
import numpy as np
import cv2
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score, StratifiedKFold, GridSearchCV
from skimage.feature import hog
from skimage.transform import resize
from joblib import Parallel, delayed
import imutils
import utils
from imgaug import augmenters as iaa
import concurrent.futures
from imgaug.augmentables.bbs import BoundingBox, BoundingBoxesOnImage
import imgaug as ia
import imgaug.augmenters as iaa
import pickle

train_image_dir = 'C:/Users/ElbSo/Desktop/SY32/Projet/clone/dataset/train/images/'
train_label_dir = 'C:/Users/ElbSo/Desktop/SY32/Projet/clone/dataset/train/labels/'
test_image_dir = 'C:/Users/ElbSo/Desktop/SY32/Projet/clone/dataset/val/images/'
test_label_dir = 'C:/Users/ElbSo/Desktop/SY32/Projet/clone/dataset/val/labels/'
lights_image_dir='C:/Users/ElbSo/Desktop/SY32/Projet/clone/dataset/train/feux'

def extract_hog_features(image):
    image = (image * 255).astype(np.uint8)  # Convert to 8-bit unsigned integer
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image = cv2.resize(image, (64, 64))
    features, _ = hog(image, orientations=9, pixels_per_cell=(8, 8),
                      cells_per_block=(2, 2), block_norm='L2-Hys',
                      visualize=True, transform_sqrt=True)
    return features


def augment_image_and_bboxes(image, bboxes):
    # Define the augmentation sequence
    aug = iaa.Sequential([
        iaa.Fliplr(0.5),  # horizontal flip 50% of the time
        iaa.Flipud(0.2),  # vertical flip 20% of the time
        iaa.Affine(rotate=(-25, 25)),  # rotate between -25 and 25 degrees
        iaa.Affine(scale=(0.8, 1.2)),  # scale images to 80-120% of their size
        iaa.AdditiveGaussianNoise(scale=(10, 60)),  # add Gaussian noise
        iaa.Multiply((0.8, 1.2)),  # change brightness
        iaa.PerspectiveTransform(scale=(0.01, 0.4)),  # apply perspective transformations
        iaa.Resize({"height": image.shape[0], "width": image.shape[1]})  # resize back to original dimensions
    ])
    
    bbs = BoundingBoxesOnImage([
        BoundingBox(x1=row['x1'], y1=row['y1'], x2=row['x2'], y2=row['y2']) for _, row in bboxes.iterrows()
    ], shape=image.shape)
    
    image_aug, bbs_aug = aug(image=image, bounding_boxes=bbs)
    bbs_aug = bbs_aug.remove_out_of_image().clip_out_of_image()
    
    augmented_bboxes = []
    for bb in bbs_aug.bounding_boxes:
        augmented_bboxes.append([bb.x1, bb.y1, bb.x2, bb.y2])
    
    return image_aug, augmented_bboxes


def augment_image_and_bboxes_lights(image, bboxes):
    aug = iaa.Sequential([
        iaa.Fliplr(0.5),
        iaa.Flipud(0.2),
        iaa.Affine(rotate=(-25, 25)),
        iaa.Affine(scale=(0.8, 1.2)),
        iaa.AdditiveGaussianNoise(scale=(10, 60)),
        iaa.Multiply((0.8, 1.2)),
        iaa.PerspectiveTransform(scale=(0.01, 0.4)),
        iaa.Resize({"height": image.shape[0], "width": image.shape[1]})
    ])
    
    bbs = BoundingBoxesOnImage([
        BoundingBox(x1=row['x1'], y1=row['y1'], x2=row['x2'], y2=row['y2']) for _, row in bboxes.iterrows()
    ], shape=image.shape)
    
    image_aug, bbs_aug = aug(image=image, bounding_boxes=bbs)
    bbs_aug = bbs_aug.remove_out_of_image().clip_out_of_image()
    
    augmented_bboxes = []
    for bb in bbs_aug.bounding_boxes:
        augmented_bboxes.append([bb.x1, bb.y1, bb.x2, bb.y2])
    
    return image_aug, augmented_bboxes




# Fonction pour générer des fenêtres positives
def genere_fenetre_pos(image, row, positive_descriptors):
    x1, y1, x2, y2, label = row['x1'], row['y1'], row['x2'], row['y2'], row['label']
    x1, y1, x2, y2 = round(x1), round(y1), round(x2), round(y2)
    crop_img = image[y1:y2, x1:x2]

    if crop_img.size == 0:
        print("Warning: crop_img is empty. Skipping...")
        return

    if crop_img.shape[0] < 8 or crop_img.shape[1] < 8:
        print("Warning: crop_img is too small. Skipping...")
        return

    features = extract_hog_features(crop_img)
    if features.size == 0:
        print("Warning: descriptor is empty. Skipping...")
        return

    positive_descriptors.append(features)
    return crop_img


def generate_fenetre_neg(image, negative_descriptors, positive_windows):
    height, width, _ = image.shape
    for _ in range(50):  # 50 fenêtres négatives par image
        for attempt in range(100):  # Limiter à 100 tentatives pour trouver une fenêtre négative valide
            x1 = np.random.randint(0, width - 100)  # largeur de la fenêtre 100
            y1 = np.random.randint(0, height - 100)
            x2 = x1 + 100
            y2 = y1 + 100
            crop_img = image[y1:y2, x1:x2]

            # Vérifier si la fenêtre négative chevauche une fenêtre positive
            overlap = False
            if not positive_windows.empty:
                for idx, row in positive_windows.iterrows():
                    px1, py1, px2, py2, label = row['x1'], row['y1'], row['x2'], row['y2'], row['label']
                    if not (x2 <= px1 or x1 >= px2 or y2 <= py1 or y1 >= py2):
                        overlap = True
                        break
            
            if overlap:
                continue  # Si chevauchement, essayer une nouvelle fenêtre

            if crop_img.size == 0 or crop_img is None:
                print(f"Image recadrée vide pour les fenêtres négatives : ({x1}, {y1}, {x2}, {y2})")
                continue
            if crop_img.shape[0] <= 0 or crop_img.shape[1] <= 0:
                print(f"Fenêtre négative a des dimensions invalides: ({x1}, {y1}, {x2}, {y2})")
                continue
            
            features = extract_hog_features(crop_img)
            negative_descriptors.append(features)
            break




from skimage.transform import pyramid_gaussian

def sliding_window(image, step_size, window_size):
    for y in range(0, image.shape[0] - window_size[1], step_size):
        for x in range(0, image.shape[1] - window_size[0], step_size):
            yield (x, y, image[y:y + window_size[1], x:x + window_size[0]])

def pyramid(image, scale=1.5, minSize=(30, 30)):

	yield image


	while True:

		w = int(image.shape[1] / scale)
		image = imutils.resize(image, width=w)


		if image.shape[0] < minSize[1] or image.shape[1] < minSize[0]:
			break

		yield image
 

def pyramid_window(image,best_rf,window_size,step_size,downscale):
    detections=[]
    scale=1
    for resized in pyramid(image, scale=downscale):
        if resized.shape[0] < window_size[1] or resized.shape[1] < window_size[0]:
            break

        for (x, y, window) in sliding_window(resized, step_size, window_size):
            if window.shape[0] != window_size[1] or window.shape[1] != window_size[0]:
                continue

            features = extract_hog_features(window)
            features = features.reshape(1, -1)
            pred = best_rf.predict(features)

            if pred == 1:
                x_original = int(x * scale)
                y_original = int(y * scale)
                window_width = int(window_size[0] * scale)
                window_height = int(window_size[1] * scale)
                detections.append((x_original, y_original, window_width, window_height))

        scale *= downscale
        print(f'scale : {scale}')

    return detections


def IoU(b1, b2):
    # b1, b2 : [i, j, h, l]
    # coordonnées des coins supérieurs gauches
    x1, y1 = b1[0], b1[1]
    x2, y2 = b2[0], b2[1]
    # coordonnées des coins inférieurs droits
    x1b, y1b = b1[0] + b1[2], b1[1] + b1[3]
    x2b, y2b = b2[0] + b2[2], b2[1] + b2[3]
    # coordonnées des coins supérieurs droits
    x1m, y1m = b1[0], b1[1] + b1[3]
    x2m, y2m = b2[0], b2[1] + b2[3]
    # coordonnées des coins inférieurs gauches
    x1n, y1n = b1[0] + b1[2], b1[1]
    x2n, y2n = b2[0] + b2[2], b2[1]
    # coordonnées des coins de l'intersection
    xA = max(x1, x2)
    yA = max(y1, y2)
    xB = min(x1b, x2b)
    yB = min(y1b, y2b)
    # calcul de l'aire de l'intersection
    interArea = max(0, xB - xA) * max(0, yB - yA)
    # calcul de l'aire de l'union
    boxAArea = b1[2] * b1[3]
    boxBArea = b2[2] * b2[3]
    iou = interArea / float(boxAArea + boxBArea - interArea)
    return iou
'''Pour chaque image, supprimer des prédictions les doublons (boîtes englobantes ayant une aire de
recouvrement supérieure à 0.5) en ne gardant que les boîtes de score maximal (technique NMS :
Non Maximum Suppression). En principe, cette procédure est appliquée comme post-traitement
au détecteur'''

def filtre_nms(results_df, tresh_iou=0.5):
    results_df[['x', 'y', 'w', 'h', 'score']] = results_df[['x', 'y', 'w', 'h', 'score']].astype(float)
    results_out = np.empty((0, 6))  # Initialize an empty array for the output
    unique_images = results_df['image_name'].unique()
    
    for image_name in unique_images:  # Process image by image
        image_results = results_df[results_df['image_name'] == image_name]
        results_in_i = image_results[['x', 'y', 'w', 'h', 'score']].values
        results_in_i = np.column_stack((image_results.index.values, results_in_i))  # Include original index for reference
        
        # Sort boxes by confidence score in descending order
        results_in_i = results_in_i[np.argsort(results_in_i[:, 5])[::-1]]
        
        # List of boxes to keep after NMS
        results_out_i = np.empty((0, 6))
        results_out_i = np.vstack((results_out_i, results_in_i[0]))  # Always keep the first (highest score) box
        
        # Compare each box to the ones that are already kept
        for n in range(1, len(results_in_i)):
            keep = True
            for m in range(len(results_out_i)):
                if IoU(results_in_i[n, 1:5], results_out_i[m, 1:5]) > tresh_iou:
                    keep = False
                    break
            if keep:
                results_out_i = np.vstack((results_out_i, results_in_i[n]))
        
        # Add the boxes for this image to the total results
        results_out = np.vstack((results_out, results_out_i))
    
    # Convert the results back to a DataFrame
    filtered_results = results_df.loc[results_out[:, 0].astype(int)]
    
    return filtered_results

def save_detections_to_csv(detections,image, image_name, csv_path, model):
    rows = []
    for (x, y, w, h) in detections:
        crop_img = image[y:y+h, x:x+w]
        if crop_img.size > 0:
            features = extract_hog_features(crop_img)
            score = model.predict_proba([features])[0][1]  # Get the score for the positive class
            rows.append([image_name, x, y, w, h, score])
    df = pd.DataFrame(rows, columns=['image_name', 'x', 'y', 'w', 'h', 'score'])
    df.to_csv(csv_path, mode='a', header=not os.path.exists(csv_path), index=False)

def load_ground_truth(image_name, label_dir):
    csv_path = os.path.join(label_dir, image_name.replace('.jpg', '.csv'))
    if not os.path.exists(csv_path):
        return pd.DataFrame(columns=['x1', 'y1', 'x2', 'y2', 'label'])
    return pd.read_csv(csv_path, header=None, names=['x1', 'y1', 'x2', 'y2', 'label'])

def compare(results_in_filtre, labels_in, tresh_iou=0.5):
    vpfp = np.full((results_in_filtre.shape[0]), False)
    detectes = np.full((labels_in.shape[0]), False)

    unique_ids = np.unique(results_in_filtre[:, 0])
    for i in unique_ids:  # image by image
        labels_in_i = labels_in[labels_in[:, 0] == i]
        detectes_in_i = detectes[labels_in[:, 0] == i]
        results_in_i = results_in_filtre[results_in_filtre[:, 0] == i]
        vpfp_in_i = vpfp[results_in_filtre[:, 0] == i]

        for n in range(labels_in_i.shape[0]):
            found = False
            m = 0
            while not found and m < results_in_i.shape[0]:
                if not vpfp_in_i[m] and IoU(labels_in_i[n][1:5].astype(float), results_in_i[m][1:5].astype(float)) > tresh_iou:
                    vpfp_in_i[m] = True
                    detectes_in_i[n] = True
                    found = True
                m += 1

        vpfp[results_in_filtre[:, 0] == i] = vpfp_in_i
        detectes[labels_in[:, 0] == i] = detectes_in_i

    return vpfp, detectes

def load_existing_descriptors(pos_descriptors_path, neg_descriptors_path):
    if os.path.exists(pos_descriptors_path):
        with open(pos_descriptors_path, 'rb') as pos_file:
            pos_descriptors = pickle.load(pos_file)
    else:
        pos_descriptors = []

    if os.path.exists(neg_descriptors_path):
        with open(neg_descriptors_path, 'rb') as neg_file:
            neg_descriptors = pickle.load(neg_file)
    else:
        neg_descriptors = []

    return pos_descriptors, neg_descriptors

def save_descriptors(pos_descriptors, neg_descriptors, pos_descriptors_path, neg_descriptors_path):
    with open(pos_descriptors_path, 'wb') as pos_file, open(neg_descriptors_path, 'wb') as neg_file:
        pickle.dump(pos_descriptors, pos_file)
        pickle.dump(neg_descriptors, neg_file)

def process_results(results_csv_path, label_dir, test_image_dir, pos_descriptors_path, neg_descriptors_path):
    results = pd.read_csv(results_csv_path)

    # Ensure correct data types
    results[['x', 'y', 'w', 'h', 'score']] = results[['x', 'y', 'w', 'h', 'score']].astype(float)

    # Load existing descriptors
    pos_descriptors, neg_descriptors = load_existing_descriptors(pos_descriptors_path, neg_descriptors_path)
    
    for image_name in results['image_name'].unique():
        image_results = results[results['image_name'] == image_name]
        ground_truth = load_ground_truth(image_name, label_dir)

        # Ensure ground truth data is also of correct type
        ground_truth[['x1', 'y1', 'x2', 'y2']] = ground_truth[['x1', 'y1', 'x2', 'y2']].astype(float)

        # Prepare ground truth in the required format for comparison
        ground_truth_data = np.column_stack((
            np.full((ground_truth.shape[0], 1), image_name),  # Add image_name column
            ground_truth[['x1', 'y1', 'x2', 'y2']].values
        ))

        vpfp, detectes = compare(image_results.values, ground_truth_data)

        false_positives = image_results[~vpfp]
        true_positives = image_results[vpfp]

        for _, row in false_positives.iterrows():
            image_path = os.path.join(test_image_dir, row['image_name'])
            x, y, w, h = int(row['x']), int(row['y']), int(row['w']), int(row['h'])
            image = cv2.imread(image_path)
            crop_img = image[y:y+h, x:x+w]
            if crop_img.size > 0:
                features = extract_hog_features(crop_img)
                neg_descriptors.append(features)

        for _, row in true_positives.iterrows():
            image_path = os.path.join(test_image_dir, row['image_name'])
            x, y, w, h = int(row['x']), int(row['y']), int(row['w']), int(row['h'])
            image = cv2.imread(image_path)
            crop_img = image[y:y+h, x:x+w]
            if crop_img.size > 0:
                features = extract_hog_features(crop_img)
                pos_descriptors.append(features)

    # Save updated descriptors
    save_descriptors(pos_descriptors, neg_descriptors, pos_descriptors_path, neg_descriptors_path)

def add_lights(lights_image_dir,pos_descriptors_path,neg_descriptors_path):
    pos_descriptors, neg_descriptors = load_existing_descriptors(pos_descriptors_path, neg_descriptors_path)
    for light_img_file in os.listdir(lights_image_dir):
        light_img_path = os.path.join(lights_image_dir, light_img_file)
        ligh_image = cv2.imread(light_img_path)
        features= extract_hog_features(ligh_image)
        pos_descriptors.append(features)
    with open('pos_descriptors_lights.pkl', 'wb') as pos_file, open('neg_descriptors_lights.pkl', 'wb') as neg_file:
        pickle.dump(pos_descriptors, pos_file)
        pickle.dump(neg_descriptors, neg_file)
    

'''
def pyramid_window(image, best_rf, step_size, window_size):
    detections = []
    scale = 1

    def resize_image(image, scale_factor):
        width = int(image.shape[1] / scale_factor)
        height = int(image.shape[0] / scale_factor)
        return cv2.resize(image, (width, height), interpolation=cv2.INTER_LINEAR)

    resized_image = image
    while True:
        if resized_image.shape[0] < window_size[1] or resized_image.shape[1] < window_size[0]:
            break

        for (x, y, window) in sliding_window(resized_image, step_size, window_size):
            if window.shape[0] != window_size[1] or window.shape[1] != window_size[0]:
                continue

            features = extract_hog_features(window)
            features = features.reshape(1, -1)
            pred = best_rf.predict(features)

            if pred == 1:
                x_original = int(x * scale)
                y_original = int(y * scale)
                window_width = int(window_size[0] * scale)
                window_height = int(window_size[1] * scale)
                detections.append((x_original, y_original, window_width, window_height))

        scale *= downscale
        resized_image = resize_image(image, scale)
        print(f'Scale level: {scale}')
    
    return detections
'''
