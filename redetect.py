
from utils import *
import pickle
window_size_rectangle = (256, 128) 
step_size = 15
downscale = 1.5
window_size = (100, 100) 



# Load the trained model
with open('retrained_rf_model.pkl', 'rb') as model_file:
    rf = pickle.load(model_file)


results_csv_path = 'results_train.csv'

for test_img_file in os.listdir(test_image_dir):
    test_img_path = os.path.join(test_image_dir, test_img_file)
    test_image = cv2.imread(test_img_path)
    if test_image is not None:
        print('en detection')
        detections = pyramid_window(test_image, rf, window_size, step_size, downscale)
        
        # Save detections to CSV
        save_detections_to_csv(detections,test_image, test_img_file, results_csv_path, rf)

        for (x, y, w, h) in detections:
            cv2.rectangle(test_image, (x, y), (x + w, y + h), (0, 255, 0), 2)
        cv2.imshow('Detection', test_image)  # Afficher l'image avec les détections
        cv2.waitKey(3000)
    else:
        print(f"Image de test non trouvée ou non valide : {test_img_path}")
'''
# Apply NMS on the saved detection results
results_df = pd.read_csv(results_csv_path, header=0)  # Read CSV with header

# Ensure the columns are of correct data types
results_df[['x', 'y', 'w', 'h', 'score']] = results_df[['x', 'y', 'w', 'h', 'score']].astype(float)

filtered_results = filtre_nms(results_df)
filtered_results.to_csv('filtered_results_train.csv', index=False)'''